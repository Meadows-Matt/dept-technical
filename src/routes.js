import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta';

import Homepage from '@/views/Homepage.vue';
import Contact from '@/views/Contact.vue';

Vue.use(Router);
Vue.use(Meta)

export default new Router({
    mode: 'history',
    routes: [
        {
          path: '/',
          name: 'Homepage',
          component: Homepage,
          meta: {
            fixedHeader: true,
            navcolour: 'light'
          }
        },
        {
            path: '/contact',
            name: 'Contact',
            component: Contact,
            meta: {
              title: 'Contact Us',
              fixedHeader: false,
              navcolour: 'dark'
            }
          }
    ]
})